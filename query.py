from api import WsdlInsertAPI, WsdlSelectAPI
import six
from . import Config


class BaseQuery(object):
    def __init__(self, model, method=None):
        self.model = model
        self.method = method

    def get_handler(self, meta_url):
        if isinstance(meta_url, dict):
            url = meta_url.get(self.identifier)
        else:
            url = meta_url

        if self.identifier == 'INSERT':
            return WsdlInsertAPI(url['ENDPOINT'], Config.auth)
        else:
            return WsdlSelectAPI(url['ENDPOINT'], Config.auth)

    @classmethod
    def _build_wsdl_object(cls, type_factory, data):
        request = {}
        for k, v in data.iteritems():
            if isinstance(v, dict) and '_api_type' in v:
                api_type = v.pop('_api_type')
                request[k] = getattr(type_factory, api_type)(**cls._build_wsdl_object(type_factory, v))
            else:
                request[k] = v
        return request


class SelectQuery(BaseQuery):
    identifier = 'SELECT'

    def __init__(self, model, *args, **kwargs):
        super(SelectQuery, self).__init__(model, 'GET')

    def parse_result(self, reference, result):
        pairs = {
            self.model._meta.pk_name: self.model.get_pk_field().python_value(reference),
            'history': [r.EstadoDescripcion for r in result]
        }
        return pairs

    def get(self, reference):
        handler = self.get_handler(self.model._meta.url)
        request = getattr(self.model, self.model._meta.url[self.identifier]['REQUEST_BUILD_CALLBACK'])(reference)
        result = handler.request(self.model._meta.url[self.identifier]['METHOD'], **request)
        if not result:
            return None

        return self.model(**dict(self.parse_result(reference, result)))


class InsertQuery(BaseQuery):
    identifier = 'INSERT'

    def __init__(self, model, idempotency_key=None, **kwargs):
        self.insert_query = kwargs
        self.idempotency_key = idempotency_key
        super(InsertQuery, self).__init__(model, 'POST')

    def parse_insert(self):
        pairs = {}
        for k, v in six.iteritems(self.insert_query):
            field = self.model._meta.get_field_by_name(k)

            if field.required or v is not None:
                pairs[field.api_name] = field.api_value(v)

        return pairs

    def parse_result(self, result, model_klass=None):
        pairs = {}
        model_klass = model_klass or self.model

        for api_name, field_name in model_klass._meta.api_names.items():
            field = model_klass._meta.get_field_by_name(field_name)
            if result and api_name in result:
                pairs[field_name] = field.python_value(result[api_name])

        return pairs

    def execute(self):
        handler = self.get_handler(self.model._meta.url)
        data = self.parse_insert()
        request = getattr(self.model, self.model._meta.url[self.identifier]['REQUEST_BUILD_CALLBACK'])(handler.type_factory, data)

        result = handler.request(self.model._meta.url[self.identifier]['METHOD'], *request)
        return dict(self.parse_result(result))


class UpdateQuery(BaseQuery):
    pass


class ActionQuery(BaseQuery):
    pass
