class Config:
    auth = None
    debug = False
    base_url = None


class Mrw(object):
    def __init__(self):
        self.config = Config

    def init_app(self, app):
        self.config.auth = app.config['MRW_SETTINGS']['AUTH']
        self.config.base_url = app.config['MRW_SETTINGS']['HOST']
        self.config.debug = app.config['DEBUG']

