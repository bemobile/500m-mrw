# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import requests
from exceptions import APIError, DecodeError
try:
    import urllib.parse as urlrequest
except ImportError:
    import urllib as urlrequest

requests_session = requests.Session()


import zeep


class WsdlBaseAPI(object):
    def __init__(self, specification, headers):
        from . import Config
        self.client = zeep.Client(wsdl=self._absolute_url(specification))
        self.headers = headers
        self.debug = Config.debug
        self._set_logging()

    @property
    def type_factory(self):
        return self.client.type_factory('ns0')

    def _create_apierror(self, result, method=None, headers=None):
        error = result.Mensaje
        raise APIError(error, content=result, headers=headers, url=method)

    def _create_decodeerror(self, result, method=None, error=None):
        raise DecodeError(result, content=error, url=method)

    def _set_logging(self):
        if not self.debug:
            return

        import logging.config
        logging.config.dictConfig({
            'version': 1,
            'formatters': {
                'verbose': {
                    'format': '%(name)s: %(message)s'
                }
            },
            'handlers': {
                'console': {
                    'level': 'DEBUG',
                    'class': 'logging.StreamHandler',
                    'formatter': 'verbose',
                },
            },
            'loggers': {
                'zeep.transports': {
                    'level': 'DEBUG',
                    'propagate': True,
                    'handlers': ['console'],
                },
            }
        })


class WsdlInsertAPI(WsdlBaseAPI):
    def __init__(self, specification, headers):
        super(WsdlInsertAPI, self).__init__(specification, headers)

    def _absolute_url(self, url):
        from . import Config
        pattern = '%s%s?wsdl'
        return pattern % (Config.base_url['INSERT'], url)

    def request(self, method, *args):
        auth = self.type_factory.AuthInfo(**self.headers)
        result = getattr(self.client.service, method)(*args, _soapheaders={'AuthInfo': auth})
        if self.debug:
            print result
        try:
            if result.Estado == '0':
                self._create_apierror(result, method=method, headers=auth)
            return result
        except ValueError as e:
            self._create_decodeerror(result, method=method, error=e.message)


class WsdlSelectAPI(WsdlBaseAPI):
    def __init__(self, specification, headers):
        super(WsdlSelectAPI, self).__init__(specification, headers)

    def _absolute_url(self, url):
        from . import Config
        pattern = '%s%s?wsdl'
        return pattern % (Config.base_url['SELECT'], url)

    def request(self, method, **kwargs):
        result = getattr(self.client.service, method)(**kwargs)
        if self.debug:
            print result
        try:
            if not result.Seguimiento.Abonado:
                return None
            return result.Seguimiento.Abonado[0].SeguimientoAbonado.Seguimiento
        except ValueError as e:
            self._create_decodeerror(result, method=method, error=e.message)


class RequestResult(object):
    def __init__(self, status_code):
        self.status_code=status_code
